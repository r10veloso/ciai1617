var StudentDetails = React.createClass({
	render: function() {
		return (
			<div className="row">
			<div className="col-md-1">{this.props.id}</div>
			<div className="col-md-2">{this.props.name}</div>
			<div className="col-md-1">{this.props.age}</div>
			</div>
		)
	}
});


var CompleteStudent = React.createClass({
	render: function() {
		return (
			<div>
			<StudentDetails name={this.props.name} id={this.props.id} age={this.props.age}/>
			</div>
		)
	}
})


var AddStudent = React.createClass({
	getInitialState: function() {		
		return {name: '', age: ''};
	},

	render: function() {
		return (
			<div>
				<h3>Add a new student</h3>
				<form onSubmit={this.handleSubmit}>
					<p>
					<label htmlFor="students_name">Name:</label>
					<input id="students_name" ref="nameInput" onChange={this.handleChangeName} value={this.state.name} />
					<span> </span>
					<label htmlFor="students_age">Age:</label>
					<input id="students_age" onChange={this.handleChangeAge} value={this.state.age} />
					<span> </span>
					<button>{'Add ' + this.state.name }</button>
					</p>
				</form>
			</div>
		);
	},

	handleChangeName(e) {
		this.setState({name: e.target.value});
	},
	
	handleChangeAge(e) {
		this.setState({age: e.target.value});
	},

	handleSubmit(e) {
		e.preventDefault();
		var newStudent = {
				name: this.state.name,
				age: this.state.age,
				id: this.props.number_of_students + 1,
		};
		this.submit(newStudent);
		this.setState(() => ({name: '', age:''}));
	},
	
	submit(student) {
	    $.ajax({
		      url: "/students",
		      type: "POST",
		      data: JSON.stringify(student),
		      contentType: 'application/json',
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		        student.id = data;
				this.props.appendStudent(student);
				this.refs.nameInput.focus();
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	}
});



var StudentsList = React.createClass({
	getInitialState: function() {
	    return {data: []};
	  },
	  
	componentDidMount: function() {
	    $.ajax({
	      url: "/students",
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	        this.setState({data: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	  },
	
	render: function() {
		var studentNodes = this.state.data.map(function(student) {
			return (
					<CompleteStudent key={student.id} name={student.name}
					id={student.id} age={student.age}/>
			);
		});
    return (
      <div className="container">
      	<h1>Student List</h1>
        {studentNodes}
        <AddStudent appendStudent={this.appendStudent}/>
      </div>
    );
  },
  
  appendStudent(student) {
	  this.setState((prevstate) => ({ data: prevstate.data.concat(student) }));
  }
});


